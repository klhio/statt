/*
 * Copyright (C) 2023  Maciej Sopylo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * statt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Example 1.0
import Qt.labs.settings 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

ApplicationWindow {
  visible: true

  header: ToolBar {
    RowLayout {
      anchors.fill: parent

      Label {
        text: "Stat Tracker"
        elide: Label.ElideRight
        horizontalAlignment: Qt.AlignLeft
        verticalAlignment: Qt.AlignVCenter
        font.pixelSize: FontUtils.sizeToPixels("large")
        Layout.fillWidth: true
        Layout.leftMargin: units.gu(1)
        Layout.rightMargin: units.gu(1)
      }
    }
  }

  Page {
    anchors.fill: parent

    ColumnLayout {

      // spacing: units.gu(2)

      anchors {
        // margins: units.gu(2)
        top: parent.top
        left: parent.left
        right: parent.right
        bottom: parent.bottom
      }

      Item {
        Layout.fillHeight: true
      }

      Label {
        id: label

        Layout.alignment: Qt.AlignHCenter
        text: i18n.tr('Press the button below and check the logs!')
      }

      Button {
        Layout.alignment: Qt.AlignHCenter
        text: i18n.tr('Press here!')
        onClicked: Example.speak()
      }

      Item {
        Layout.fillHeight: true
      }
    }
  }
}
